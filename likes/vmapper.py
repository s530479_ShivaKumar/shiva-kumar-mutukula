# FOR EACH VIDEO, COUNT COMMENTS.
# RAW DATA HAS 1 COMMENT PER LINE.
# 
# STEP 1. MAP each line, output video id and 1 for this comment

# open data.csv for reading as a file stream named i
# open  MapperOutput.csv for writing as a file stream named o
f = open("data.csv","r")  # open file, read-only
o = open("MapperOutput.csv", "w") # open file, write


# for each line in your input file stream
for line in f:  
# strip the line, split it by the delimiter into a list named 'data'
    data = line.strip().split(",")
# if len(data) is equal to 4 (remember the colon)
    if len(data) == 4 and len(data[1])>0:
        # assign data to four named variables
    # use o.write to output the id comma 1
        o.write("{0},{1}\n".format(data[0].strip(), 1))
    
# close your file streams
f.close() #close the file
o.close() #close the file


