# FOR EACH VIDEO, COUNT COMMENTS.
# SORTED DATA HAS 1 COMMENT PER LINE, ALL VIDEOIDS TOGETHER
# 
# STEP 3. REDUCE videoID and counts by summing the counts into 1 record per video

# open sortedoutput.csv for reading as a file stream named s
input = open("sortedoutput.csv","r")
# open reducerout.csv for writing as a file stream named r
output = open("reducerout.csv","w")
# initialize thisKey to an empty string
thisKey = ""
# initialize thisValue to 0
thisValue = 0.0
count =0
maxValue =0
maxVideo_Id = ""
# Iterating through the input
for line in input:
    # split the csv record to a tuple
    record = line.strip().split(",")
    # checking for dirty data with length
    if len(record) == 2:
        Video_id, value = record
        if Video_id.strip() != thisKey:
            if thisKey:
                # writing into the output file
                output.write(thisKey.strip() + '\t' + str(thisValue)+'\n')
                if thisValue > maxValue:
                  maxValue = thisValue
                  maxVideo_Id = thisKey
            # when the key is changed, starts again
            thisKey = Video_id.strip()
            thisValue = 0.0
        # Aggregating the results 
        thisValue += float(value)
# Write the result into the output file
output.write(thisKey.strip() + '\t' + str(thisValue)+'\n')
print(maxVideo_Id,maxValue)
input.close()
output.close()
