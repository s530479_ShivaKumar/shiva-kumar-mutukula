# FOR EACH VIDEO, COUNT COMMENTS.
# MAPPER OUTPUT HAS ID, 1
# 
# STEP 2. Mimic sort-and-shuffle in MR

# open MapperOutput.csv for reading as a file stream named m
# open sortedoutput.csv for writing as a file stream named s
mapperinput = open("MapperOutput.csv", "r") # open file read only
sortedoutput = open("sortedoutput.csv", "w") # write into file

# use readlines method of the file stream object to create a list called lines
lines = mapperinput.readlines() # reading the each line

# call the list sort method on your lines to sort in place (do not reassign to another variable)
lines.sort() #sort the data

# for every line in lines (remember the colon)
for line in lines:
    # use s.write to output the line
	sortedoutput.write(line)
# close file streams

mapperinput.close() # close the mapper file
sortedoutput.close() # close the sortedoutput file




